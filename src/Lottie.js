import React, {Component} from 'react'
import LottieView from 'lottie-react-native'
import check_animation from '../check_animation'

export default class Lottie extends Component {

    render() {
        return (
            <LottieView
                source={check_animation}
                autoPlay
                loop
            />
        )}

}