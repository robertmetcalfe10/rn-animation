import { createBottomTabNavigator } from "react-navigation"
import Map from "./Map"
import Grid from "./Grid"
import Lottie from "./Lottie"


const AppNavigator = createBottomTabNavigator({
    Map: {
        screen: Map,
    },
    Grid: {
        screen: Grid,
    },
    Lottie: {
        screen: Lottie,
    }
});

export default AppNavigator;
