import React, { Component } from 'react'
import AppNavigator from './src/appNavigator'

export default class App extends Component {
    render () {
        return (
            <AppNavigator/>
        )
    }
}